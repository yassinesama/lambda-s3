# npm install async gm

# zip -r function.zip .
# # create-function
# aws lambda create-function --function-name CreateThumbnail \
# --zip-file fileb://function.zip --handler index.handler --runtime nodejs8.10 \
# --timeout 10 --memory-size 1024 \
# --role arn:aws:iam::039027149648:role/lambda-s3-role --profile yassine --region eu-west-2

# # if want to upadte timeout
# aws lambda update-function-configuration --function-name CreateThumbnail --timeout 30

# # UPDATE CODE
# zip -r function.zip .
# aws lambda update-function-code --function-name CreateThumbnail --zip-file fileb://function.zip --profile yassine


# # test the lambda function by sending a json
# aws lambda invoke --function-name CreateThumbnail --invocation-type Event \
# --payload file://inputfile.txt outputfile.txt --profile yassine --region eu-west-2

.PHONY: up
up:
	zip -r function.zip . --exclude=*.DS_Store* --exclude=Makefile --exclude=media/*
	aws lambda update-function-code --function-name CreateThumbnail --zip-file fileb://function.zip --profile yassine --region eu-west-2

.PHONY: invoke
invoke: up
	# execute the lambda fucntion
	aws lambda invoke --function-name CreateThumbnail --invocation-type Event \
--payload file://inputfile.txt outputfile.txt --profile yassine --region eu-west-2

.PHONY: redo-media
redo-media:
	rm -rf media
	aws s3 cp --recursive s3://automatic-meal-planer/media media/ --profile yassine
	aws s3 rm --recursive s3://automatic-meal-planer/media/ --profile yassine
	aws s3 cp --recursive media s3://automatic-meal-planer/media/ --profile yassine



# aws lambda add-permission --function-name CreateThumbnail --principal s3.amazonaws.com \
# --statement-id id123456 --action "lambda:InvokeFunction" \
# --source-arn arn:aws:s3:::automatic-meal-planer \
# --source-account 039027149648 --profile yassine